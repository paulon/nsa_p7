import logging

import bot

import secrets

# Configure les logs
# https://discordpy.readthedocs.io/en/stable/logging.html#logging-setup
logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    client = bot.Bot(delimiter="!")
    client.run(secrets.token)
