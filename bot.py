import discord
import importlib
import json

import modules_manager
import settings

class Config():
    def __init__(self, filename):
        self.filename = filename
        try:
            with open(self.filename, "r") as file:
                self.from_dict(json.load(file))
        except FileNotFoundError:
            self.from_dict({})
            self.dump()

    def dump(self):
        with open(self.filename, "w") as file:
            file.write(json.dumps(self.to_dict(), indent=True))

    def from_dict(self, config_dict):
        self.modules = config_dict.get("modules", {})
        self.activity = config_dict.get("activity", "")

    def to_dict(self):
        return {
            "modules": self.modules,
            "activity": self.activity
        }

class Bot(discord.Client):
    def __init__(self, delimiter, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__version__="0.1.0"
        self.__state__="alpha"
        self.delimiter = delimiter
        self.modules = settings.modules
        self.config = Config(settings.dumpfile)
        self.config.activity = settings.default_activity
        self.dump_config()
        self.reload()

    def dump_config(self):
        self.config.dump()

    def reload(self):
        importlib.reload(modules_manager)
        modules_manager.reconfigure(self)
        importlib.reload(settings)
