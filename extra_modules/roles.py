import argparse
import shlex

import extra_modules.module_template

class ArgumentParserError(Exception): pass
class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    def error(self, message):
        raise ArgumentParserError(message)


class cmd:

    @staticmethod
    def menu(module, *args, **kwargs):
        print("args:", args, "kwargs:", kwargs)
        if kwargs.get("list"):
            menus = ""
            for id, nom in module.config["menus"].items():
                menus += f"`{id}`: {nom}\n"
            if menus == "":
                return "No Menus"
            return menus.strip()

        if kwargs.get("remove"):
            module.config["menus"].pop(kwargs.get("remove")[0])
            module.dump_config()

        if kwargs.get("add"):
            i = 0
            while module.config["menus"].get(i):
                i+=1
            module.config["menus"][i] = kwargs.get("add")[0]
            module.dump_config()
            return f"Added menu {module.config['menus'][i]} with id {i}"

    @staticmethod
    def role(module, *args, **kwargs):
        print("args:", args, "kwargs:", kwargs)
        if kwargs.get("list"):
            roles = ""
            print(module.config["roles"])
            if module.config["roles"] == {}:
                return "No Roles"
            for id, values in module.config["roles"].items():
                roles += f"`{id}`: {values['role']}, {values['emoji']}, {module.config['menus'][values['id_menu']]}\n"
            return roles.strip()

        if kwargs.get("remove"):
            module.config["roles"].pop(kwargs.get("remove")[0])
            module.dump_config()

        if kwargs.get("add"):
            i = 0
            while module.config["roles"].get(i):
                i+=1
            module.config["roles"][i] = {
                "id_menu": kwargs.get("add")[0],
                "role": kwargs.get("add")[1],
                "emoji": kwargs.get("add")[2]}
            module.dump_config()
            return f"Added role {module.config['roles'][i]} with id {i}"

    @staticmethod
    def chan(module, *args, **kwargs):
        print("args:", args, "kwargs:", kwargs)
        if kwargs.get("list"):
            roles = ""
            for id, values in module.config["chans"].items():
                roles += f"`{id}`: {values['chan']} {module.config['menus'][values['id_menu']]}\n"
            if roles == "":
                return "No chans"
            return roles.strip()

        if kwargs.get("remove"):
            module.config["chans"].pop(kwargs.get("remove")[0])
            module.dump_config()

        if kwargs.get("add"):
            i = 0
            while module.config["chans"].get(i):
                i+=1
            module.config["chans"][i] = {
                "chan": kwargs.get("add")[0],
                "id_menu": kwargs.get("add")[1]}
            module.dump_config()
            return f"Added chan {module.config['chans'][i]['chan']} for menu {module.config['menus'][module.config['chans'][i]['id_menu']]} with id {i}"


class Module(extra_modules.module_template.Module):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Roles"
        self.author = "Michaël «Mikachu» PAULON"
        self.contact = "paulon@crans.org"
        self.config = self.get_config()
        self.init_config()

        self.role_parser = ArgumentParser(prog="role")
        self.role_parser.add_argument("-a", "--add",
            type=str, nargs=3,
            metavar=("menu", "role", "emoji"))
        self.role_parser.add_argument("-r", "--remove",
            type=str, nargs=1,
            metavar=("role_id"))
        self.role_parser.add_argument("-l", "--list",
            action="store_true")


        self.menu_parser = ArgumentParser(prog="menu")
        self.menu_parser.add_argument("-a", "--add",
            type=str, nargs=1,
            metavar="nom")
        self.menu_parser.add_argument("-r", "--remove",
            type=str, nargs=1,
            metavar="menu_id")
        self.menu_parser.add_argument("-l", "--list",
            action="store_true")

        self.chan_parser = ArgumentParser(prog="chan")
        self.chan_parser.add_argument("-a", "--add",
            type=str, nargs=2,
            metavar=("chan", "menu_id"))
        self.chan_parser.add_argument("-r", "--remove",
            type=str, nargs=1,
            metavar="chan_id")
        self.chan_parser.add_argument("-l", "--list",
            action="store_true")

        self.parsers = {
            "role": self.role_parser,
            "menu": self.menu_parser,
            "chan": self.chan_parser,
        }


    def init_config(self):
        if not self.config.get("menus"):
            self.config["menus"] = {}
            # "id_menu": "nom"
        if not self.config.get("roles"):
            self.config["roles"] = {}
            # "id_role": {"role":, "emoji": , "id_menu": }
        if not self.config.get("chans"):
            self.config["chans"] = {}
            # "id_chan": [liste ids menus]
        if not self.config.get("messages"):
            self.config["messages"] = {}
            # "id_message": "id_menu"

    async def apply_command(self, bot, command, message):
        command = shlex.split(command, comments=False)
        print(command)
        command, args = command[0], command[1:]
        if command in self.parsers:
            try:
                args = self.parsers[command].parse_args(args)
            except ArgumentParserError as exn:
                await message.channel.send(f"`{self.parsers[command].format_usage()}`")
                return
            await message.channel.send(getattr(cmd, command)(self, **vars(args)))


    async def on_reaction_add(self, bot, reaction, user):
        pass
    async def on_reaction_remove(self, bot, reaction, user):
        pass
