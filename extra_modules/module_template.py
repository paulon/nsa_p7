class Module():
    def __init__(self, bot):
        self.name = "Example Module"
        self.author = "Peter «Spiderman» Parker"
        self.contact = "mail@example.com"
        self.bot = bot

        self.parsers = {}

    def get_config(self):
        return self.bot.config.modules.get(self.name, {})

    def dump_config(self):
        print(self.name, "config", self.config)
        self.bot.config.modules[self.name] = self.config
        self.bot.dump_config()

    async def apply_command(self, bot, command, message):
        pass

    async def on_message(self, bot, message):
        pass

    async def on_reaction_add(self, bot, reaction, user):
        pass

    async def on_reaction_remove(self, bot, reaction, user):
        pass

    def __str__(self):
        return(f"{self.name} from {self.author}({self.contact})")
