import importlib
import traceback
import sys
import extra_modules.roles
import discord


def reconfigure(bot):
    bot.imported_modules = []

    for module in bot.modules:
        real_module = importlib.import_module(module)
        module_obj = importlib.reload(real_module).Module(bot)
        bot.imported_modules.append(module_obj)

    @bot.event
    async def on_message(message):
        if message.content.startswith(bot.delimiter) and len(message.content) > 1:
            await apply_command(bot, message.content[1:], message)

        else:
            for module in bot.imported_modules:
                await module.on_message(bot, message)

    @bot.event
    async def on_reaction_add(reaction, user):
        for module in bot.imported_modules:
            await module.on_reaction_add(bot, reaction, user)

    @bot.event
    async def on_reaction_remove(reaction, user):
        for module in bot.imported_modules:
            await module.on_reaction_remove(bot, reaction, user)



async def apply_command(bot, command, message):
    if command == "reload":
        await message.channel.send("Reloading modules !")
        try:
            bot.reload()
            await message.channel.send("Reloading complete !")
        except:
            traceback.print_exc(file=sys.stdout)
            await message.channel.send("Reloading failed !")
            await message.channel.send(f"`{traceback.format_exc()}`")

    elif command == "die":
        await message.channel.send("Bye Bye !")
        await bot.close()
        exit(0)

    elif command.startswith("infos"):
        await message.channel.send(f"**BOT**: {bot.__version__}({bot.__state__})")
        await message.channel.send(f"**MODULES**:{chr(10)}{''.join([ str(m)+chr(10) for m in bot.imported_modules])}")

    else:
        for module in bot.imported_modules:
            await module.apply_command(bot, command, message)
